import  'package:flutter/material.dart';

void main() {
  runApp(HelloWord());
}

class HelloWord extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello World', home: Text('Hello World'));
  }
}